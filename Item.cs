﻿using System.Xml.Serialization;

namespace Control_17_05
{
    [XmlRoot(ElementName = "item")]
    public class Item
    {
        [XmlElement(ElementName = "change")]
        public string Change { get; set; }
        [XmlElement(ElementName = "description")]
        public string Description { get; set; }
        //[XmlElement(ElementName = "fullname")]
        //public string Fullname { get; set; }
        //[XmlElement(ElementName = "index")]
        //public string Index { get; set; }
        [XmlElement(ElementName = "quant")]
        public string Quant { get; set; }
        [XmlElement(ElementName = "title")]
        public string Title { get; set; }
    }
}
