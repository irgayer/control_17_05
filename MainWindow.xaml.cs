﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Control_17_05
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        WebClient client;
        public MainWindow()
        {
            InitializeComponent();
            client = new WebClient();
            dateBox.Text = $"Курс валют к тенге на {DateTime.Today.Day}.{DateTime.Today.Month}.{DateTime.Today.Year}";
            RefreshCurrences();
        }

        private async void RefreshCurrences()
        {
            string xml = await client.DownloadStringTaskAsync(new Uri($"http://www.nationalbank.kz/rss/get_rates.cfm?fdate={DateTime.Today.Day}.{DateTime.Today.Month}.{DateTime.Today.Year}"));
            Rates currencies = Deserialize.XmlToRates(xml);
            dataGrid.ItemsSource = currencies.Item;

            dataGrid.Columns[0].Header = "Изменение";
            dataGrid.Columns[1].Header = "Стоимость";
            dataGrid.Columns[2].Header = "Кол-во";
            dataGrid.Columns[3].Header = "Название";
        }

        private void refreshButtonClick(object sender, RoutedEventArgs e)
        {
            dateBox.Text = $"Курс валют к тенге на {DateTime.Today.Day}.{DateTime.Today.Month}.{DateTime.Today.Year}";
            RefreshCurrences();
        }
    }
}
