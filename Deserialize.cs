﻿using System.IO;
using System.Xml.Serialization;

namespace Control_17_05
{
    public static class Deserialize
    {
        public static Rates XmlToRates(string xml)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Rates));
            Rates result = new Rates();
            using (TextReader reader = new StringReader(xml))
            {
                result = (Rates)serializer.Deserialize(reader);
            }
            return result;    
        }
    }
}
